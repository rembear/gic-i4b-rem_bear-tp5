<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql = "CREATE TABLE students (
id INT(6) AUTO_INCREMENT PRIMARY KEY, 
first_name VARCHAR(25) NOT NULL,
last_name VARCHAR(25) NOT NULL,
sex ENUM('Male','Female'),
telephone VARCHAR(15) UNIQUE,
email VARCHAR(50),
CONSTRAINT students_unique UNIQUE (telephone,email)
)";

if ($conn->query($sql) === TRUE) {
    echo "Table my_db created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>