
<?php
	$servername = 'localhost';
	$user = 'root';
	$pass = '';
	$conn = new mysqli($servername,$user,$pass);
	if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	} 



	// Create Database

	$sql = "CREATE DATABASE my_db;";

	//Execute query to create db and check db 

	if ($conn->query($sql) === TRUE) {
	    echo "database my_db is created successfully";
	} else {

	    echo "Error creating db: " . $conn->error;
	}
	
	$conn->close();
?>